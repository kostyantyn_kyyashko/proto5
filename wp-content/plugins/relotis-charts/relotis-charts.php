<?php
/*
 *	Plugin Name: Relotis charts
 *  Author: Relotis
 *  Description: meter with scale for crm
 *
 * License: GPLv3 or later
 * License URI: https://www.gnu.org/licenses/gpl-3.0.en.html
 * Version: 1.0.0
 * Text Domain: relotis-crm
 */

require_once plugin_dir_path(__FILE__).'common_chart_css_js.php';
require_once plugin_dir_path(__FILE__).'area_chart.php';
require_once plugin_dir_path(__FILE__).'area_chart3.php';
require_once plugin_dir_path(__FILE__).'../virtual_meter/virtual_meter.php';
require_once plugin_dir_path(__FILE__).'donut_chart.php';
require_once plugin_dir_path(__FILE__).'bar_chart.php';
require_once plugin_dir_path(__FILE__).'sema4.php';
/*do_shortcode('[common_chart_css_js]');
do_shortcode('[area_chart]');
do_shortcode('[virtual_meter]');
do_shortcode('[donut_chart]');
do_shortcode('[bar_chart]');
do_shortcode('[bar_chart]');*/






